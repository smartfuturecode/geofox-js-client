/**
 * Geofox Api
 * Das GEOFOX Thin Interface, im folgenden GTI genannt, ist eine REST-ähnliche Web-Service-Schnittstelle für GEOFOX.
 *
 * The version of the OpenAPI document: 38.1.0
 * Contact: api@geofox.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.GeofoxApi);
  }
}(this, function(expect, GeofoxApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new GeofoxApi.DLRequest();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('DLRequest', function() {
    it('should create an instance of DLRequest', function() {
      // uncomment below and update the code to test DLRequest
      //var instane = new GeofoxApi.DLRequest();
      //expect(instance).to.be.a(GeofoxApi.DLRequest);
    });

    it('should have the property language (base name: "language")', function() {
      // uncomment below and update the code to test the property language
      //var instane = new GeofoxApi.DLRequest();
      //expect(instance).to.be();
    });

    it('should have the property version (base name: "version")', function() {
      // uncomment below and update the code to test the property version
      //var instane = new GeofoxApi.DLRequest();
      //expect(instance).to.be();
    });

    it('should have the property filterType (base name: "filterType")', function() {
      // uncomment below and update the code to test the property filterType
      //var instane = new GeofoxApi.DLRequest();
      //expect(instance).to.be();
    });

    it('should have the property station (base name: "station")', function() {
      // uncomment below and update the code to test the property station
      //var instane = new GeofoxApi.DLRequest();
      //expect(instance).to.be();
    });

    it('should have the property stations (base name: "stations")', function() {
      // uncomment below and update the code to test the property stations
      //var instane = new GeofoxApi.DLRequest();
      //expect(instance).to.be();
    });

    it('should have the property time (base name: "time")', function() {
      // uncomment below and update the code to test the property time
      //var instane = new GeofoxApi.DLRequest();
      //expect(instance).to.be();
    });

    it('should have the property maxList (base name: "maxList")', function() {
      // uncomment below and update the code to test the property maxList
      //var instane = new GeofoxApi.DLRequest();
      //expect(instance).to.be();
    });

    it('should have the property maxTimeOffset (base name: "maxTimeOffset")', function() {
      // uncomment below and update the code to test the property maxTimeOffset
      //var instane = new GeofoxApi.DLRequest();
      //expect(instance).to.be();
    });

    it('should have the property allStationsInChangingNode (base name: "allStationsInChangingNode")', function() {
      // uncomment below and update the code to test the property allStationsInChangingNode
      //var instane = new GeofoxApi.DLRequest();
      //expect(instance).to.be();
    });

    it('should have the property useRealtime (base name: "useRealtime")', function() {
      // uncomment below and update the code to test the property useRealtime
      //var instane = new GeofoxApi.DLRequest();
      //expect(instance).to.be();
    });

    it('should have the property returnFilters (base name: "returnFilters")', function() {
      // uncomment below and update the code to test the property returnFilters
      //var instane = new GeofoxApi.DLRequest();
      //expect(instance).to.be();
    });

    it('should have the property filter (base name: "filter")', function() {
      // uncomment below and update the code to test the property filter
      //var instane = new GeofoxApi.DLRequest();
      //expect(instance).to.be();
    });

    it('should have the property serviceTypes (base name: "serviceTypes")', function() {
      // uncomment below and update the code to test the property serviceTypes
      //var instane = new GeofoxApi.DLRequest();
      //expect(instance).to.be();
    });

    it('should have the property departure (base name: "departure")', function() {
      // uncomment below and update the code to test the property departure
      //var instane = new GeofoxApi.DLRequest();
      //expect(instance).to.be();
    });

  });

}));
