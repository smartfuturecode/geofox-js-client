/**
 * Geofox Api
 * Das GEOFOX Thin Interface, im folgenden GTI genannt, ist eine REST-ähnliche Web-Service-Schnittstelle für GEOFOX.
 *
 * The version of the OpenAPI document: 38.1.0
 * Contact: api@geofox.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.GeofoxApi);
  }
}(this, function(expect, GeofoxApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new GeofoxApi.TicketListTicketVariant();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('TicketListTicketVariant', function() {
    it('should create an instance of TicketListTicketVariant', function() {
      // uncomment below and update the code to test TicketListTicketVariant
      //var instane = new GeofoxApi.TicketListTicketVariant();
      //expect(instance).to.be.a(GeofoxApi.TicketListTicketVariant);
    });

    it('should have the property ticketId (base name: "ticketId")', function() {
      // uncomment below and update the code to test the property ticketId
      //var instane = new GeofoxApi.TicketListTicketVariant();
      //expect(instance).to.be();
    });

    it('should have the property kaNummer (base name: "kaNummer")', function() {
      // uncomment below and update the code to test the property kaNummer
      //var instane = new GeofoxApi.TicketListTicketVariant();
      //expect(instance).to.be();
    });

    it('should have the property price (base name: "price")', function() {
      // uncomment below and update the code to test the property price
      //var instane = new GeofoxApi.TicketListTicketVariant();
      //expect(instance).to.be();
    });

    it('should have the property currency (base name: "currency")', function() {
      // uncomment below and update the code to test the property currency
      //var instane = new GeofoxApi.TicketListTicketVariant();
      //expect(instance).to.be();
    });

    it('should have the property ticketClass (base name: "ticketClass")', function() {
      // uncomment below and update the code to test the property ticketClass
      //var instane = new GeofoxApi.TicketListTicketVariant();
      //expect(instance).to.be();
    });

    it('should have the property discount (base name: "discount")', function() {
      // uncomment below and update the code to test the property discount
      //var instane = new GeofoxApi.TicketListTicketVariant();
      //expect(instance).to.be();
    });

    it('should have the property validityBegin (base name: "validityBegin")', function() {
      // uncomment below and update the code to test the property validityBegin
      //var instane = new GeofoxApi.TicketListTicketVariant();
      //expect(instance).to.be();
    });

    it('should have the property validityEnd (base name: "validityEnd")', function() {
      // uncomment below and update the code to test the property validityEnd
      //var instane = new GeofoxApi.TicketListTicketVariant();
      //expect(instance).to.be();
    });

  });

}));
