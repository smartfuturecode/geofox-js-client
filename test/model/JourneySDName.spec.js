/**
 * Geofox Api
 * Das GEOFOX Thin Interface, im folgenden GTI genannt, ist eine REST-ähnliche Web-Service-Schnittstelle für GEOFOX.
 *
 * The version of the OpenAPI document: 38.1.0
 * Contact: api@geofox.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.GeofoxApi);
  }
}(this, function(expect, GeofoxApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new GeofoxApi.JourneySDName();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('JourneySDName', function() {
    it('should create an instance of JourneySDName', function() {
      // uncomment below and update the code to test JourneySDName
      //var instane = new GeofoxApi.JourneySDName();
      //expect(instance).to.be.a(GeofoxApi.JourneySDName);
    });

    it('should have the property name (base name: "name")', function() {
      // uncomment below and update the code to test the property name
      //var instane = new GeofoxApi.JourneySDName();
      //expect(instance).to.be();
    });

    it('should have the property city (base name: "city")', function() {
      // uncomment below and update the code to test the property city
      //var instane = new GeofoxApi.JourneySDName();
      //expect(instance).to.be();
    });

    it('should have the property combinedName (base name: "combinedName")', function() {
      // uncomment below and update the code to test the property combinedName
      //var instane = new GeofoxApi.JourneySDName();
      //expect(instance).to.be();
    });

    it('should have the property id (base name: "id")', function() {
      // uncomment below and update the code to test the property id
      //var instane = new GeofoxApi.JourneySDName();
      //expect(instance).to.be();
    });

    it('should have the property type (base name: "type")', function() {
      // uncomment below and update the code to test the property type
      //var instane = new GeofoxApi.JourneySDName();
      //expect(instance).to.be();
    });

    it('should have the property coordinate (base name: "coordinate")', function() {
      // uncomment below and update the code to test the property coordinate
      //var instane = new GeofoxApi.JourneySDName();
      //expect(instance).to.be();
    });

    it('should have the property tariffDetails (base name: "tariffDetails")', function() {
      // uncomment below and update the code to test the property tariffDetails
      //var instane = new GeofoxApi.JourneySDName();
      //expect(instance).to.be();
    });

    it('should have the property serviceTypes (base name: "serviceTypes")', function() {
      // uncomment below and update the code to test the property serviceTypes
      //var instane = new GeofoxApi.JourneySDName();
      //expect(instance).to.be();
    });

    it('should have the property hasStationInformation (base name: "hasStationInformation")', function() {
      // uncomment below and update the code to test the property hasStationInformation
      //var instane = new GeofoxApi.JourneySDName();
      //expect(instance).to.be();
    });

    it('should have the property arrTime (base name: "arrTime")', function() {
      // uncomment below and update the code to test the property arrTime
      //var instane = new GeofoxApi.JourneySDName();
      //expect(instance).to.be();
    });

    it('should have the property depTime (base name: "depTime")', function() {
      // uncomment below and update the code to test the property depTime
      //var instane = new GeofoxApi.JourneySDName();
      //expect(instance).to.be();
    });

    it('should have the property arrDelay (base name: "arrDelay")', function() {
      // uncomment below and update the code to test the property arrDelay
      //var instane = new GeofoxApi.JourneySDName();
      //expect(instance).to.be();
    });

    it('should have the property depDelay (base name: "depDelay")', function() {
      // uncomment below and update the code to test the property depDelay
      //var instane = new GeofoxApi.JourneySDName();
      //expect(instance).to.be();
    });

    it('should have the property extra (base name: "extra")', function() {
      // uncomment below and update the code to test the property extra
      //var instane = new GeofoxApi.JourneySDName();
      //expect(instance).to.be();
    });

    it('should have the property cancelled (base name: "cancelled")', function() {
      // uncomment below and update the code to test the property cancelled
      //var instane = new GeofoxApi.JourneySDName();
      //expect(instance).to.be();
    });

    it('should have the property attributes (base name: "attributes")', function() {
      // uncomment below and update the code to test the property attributes
      //var instane = new GeofoxApi.JourneySDName();
      //expect(instance).to.be();
    });

    it('should have the property platform (base name: "platform")', function() {
      // uncomment below and update the code to test the property platform
      //var instane = new GeofoxApi.JourneySDName();
      //expect(instance).to.be();
    });

    it('should have the property realtimePlatform (base name: "realtimePlatform")', function() {
      // uncomment below and update the code to test the property realtimePlatform
      //var instane = new GeofoxApi.JourneySDName();
      //expect(instance).to.be();
    });

  });

}));
