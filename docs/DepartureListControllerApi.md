# GeofoxApi.DepartureListControllerApi

All URIs are relative to *https://gti.geofox.de*

Method | HTTP request | Description
------------- | ------------- | -------------
[**departureList**](DepartureListControllerApi.md#departureList) | **POST** /gti/public/departureList | 



## departureList

> DLResponse departureList(dLRequest)



### Example

```javascript
import GeofoxApi from 'geofox_api';
let defaultClient = GeofoxApi.ApiClient.instance;
// Configure HTTP basic authorization: APIKey1
let APIKey1 = defaultClient.authentications['APIKey1'];
APIKey1.username = 'YOUR USERNAME';
APIKey1.password = 'YOUR PASSWORD';

let apiInstance = new GeofoxApi.DepartureListControllerApi();
let dLRequest = new GeofoxApi.DLRequest(); // DLRequest | 
apiInstance.departureList(dLRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dLRequest** | [**DLRequest**](DLRequest.md)|  | 

### Return type

[**DLResponse**](DLResponse.md)

### Authorization

[APIKey1](../README.md#APIKey1)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: */*

