# GeofoxApi.Schedule

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**routeId** | **Number** |  | [optional] 
**start** | [**SDName**](SDName.md) |  | 
**dest** | [**SDName**](SDName.md) |  | 
**time** | **Number** |  | [optional] 
**footpathTime** | **Number** |  | [optional] 
**tickets** | [**[Ticket]**](Ticket.md) |  | 
**tariffInfos** | [**[TariffInfo]**](TariffInfo.md) |  | [optional] 
**scheduleElements** | [**[ScheduleElement]**](ScheduleElement.md) |  | [optional] 
**contSearchBefore** | [**ContSearchByServiceId**](ContSearchByServiceId.md) |  | [optional] 
**contSearchAfter** | [**ContSearchByServiceId**](ContSearchByServiceId.md) |  | [optional] 


