# GeofoxApi.Location

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** |  | [optional] [default to &#39;SINGLE_LINE&#39;]
**name** | **String** |  | [optional] 
**line** | [**Service**](Service.md) |  | [optional] 
**begin** | [**SDName**](SDName.md) |  | [optional] 
**end** | [**SDName**](SDName.md) |  | [optional] 
**bothDirections** | **Boolean** |  | [optional] [default to true]



## Enum: TypeEnum


* `SINGLE_LINE` (value: `"SINGLE_LINE"`)

* `ALL_LINES_OF_CARRIER` (value: `"ALL_LINES_OF_CARRIER"`)

* `COMPLETE_NET` (value: `"COMPLETE_NET"`)




