# GeofoxApi.GRResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**returnCode** | **String** |  | 
**errorText** | **String** |  | [optional] 
**errorDevInfo** | **String** |  | [optional] 
**schedules** | [**[Schedule]**](Schedule.md) |  | [optional] 
**realtimeSchedules** | [**[Schedule]**](Schedule.md) |  | [optional] 
**realtimeAffected** | **Boolean** |  | [optional] [default to false]
**individualTrack** | [**IndividualTrack**](IndividualTrack.md) |  | [optional] 



## Enum: ReturnCodeEnum


* `OK` (value: `"OK"`)

* `ERROR_ROUTE` (value: `"ERROR_ROUTE"`)

* `ERROR_COMM` (value: `"ERROR_COMM"`)

* `ERROR_CN_TOO_MANY` (value: `"ERROR_CN_TOO_MANY"`)

* `ERROR_TEXT` (value: `"ERROR_TEXT"`)

* `START_DEST_TOO_CLOSE` (value: `"START_DEST_TOO_CLOSE"`)




