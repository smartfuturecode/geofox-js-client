# GeofoxApi.IndividualTrack

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**time** | **Number** |  | [optional] 
**length** | **Number** |  | [optional] 
**type** | **String** |  | 



## Enum: TypeEnum


* `BUS` (value: `"BUS"`)

* `TRAIN` (value: `"TRAIN"`)

* `SHIP` (value: `"SHIP"`)

* `FOOTPATH` (value: `"FOOTPATH"`)

* `BICYCLE` (value: `"BICYCLE"`)

* `AIRPLANE` (value: `"AIRPLANE"`)

* `CHANGE` (value: `"CHANGE"`)

* `CHANGE_SAME_PLATFORM` (value: `"CHANGE_SAME_PLATFORM"`)




