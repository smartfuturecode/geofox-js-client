# GeofoxApi.GRRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**language** | **String** |  | [optional] [default to &#39;de&#39;]
**version** | **Number** |  | [optional] [default to 38]
**filterType** | **String** |  | [default to &#39;NO_FILTER&#39;]
**start** | [**SDName**](SDName.md) |  | 
**dest** | [**SDName**](SDName.md) |  | 
**via** | [**SDName**](SDName.md) |  | [optional] 
**time** | [**GTITime**](GTITime.md) |  | 
**timeIsDeparture** | **Boolean** |  | [optional] 
**numberOfSchedules** | **Number** |  | [optional] 
**penalties** | [**[Penalty]**](Penalty.md) |  | [optional] 
**tariffDetails** | **Boolean** |  | [optional] [default to false]
**continousSearch** | **Boolean** |  | [optional] [default to false]
**contSearchByServiceId** | [**ContSearchByServiceId**](ContSearchByServiceId.md) |  | [optional] 
**coordinateType** | **String** |  | [optional] [default to &#39;EPSG_4326&#39;]
**schedulesBefore** | **Number** |  | [optional] [default to 0]
**schedulesAfter** | **Number** |  | [optional] [default to 0]
**tariffInfoSelector** | [**[TariffInfoSelector]**](TariffInfoSelector.md) |  | [optional] 
**returnReduced** | **Boolean** |  | [optional] [default to false]
**returnPartialTickets** | **Boolean** |  | [optional] [default to true]
**realtime** | **String** |  | [optional] [default to &#39;AUTO&#39;]
**intermediateStops** | **Boolean** |  | [optional] [default to false]
**useStationPosition** | **Boolean** |  | [optional] [default to true]
**forcedStart** | [**SDName**](SDName.md) |  | [optional] 
**forcedDest** | [**SDName**](SDName.md) |  | [optional] 
**toStartBy** | **String** |  | [optional] 
**toDestBy** | **String** |  | [optional] 
**returnContSearchData** | **Boolean** |  | [optional] 



## Enum: FilterTypeEnum


* `NO_FILTER` (value: `"NO_FILTER"`)

* `HVV_LISTED` (value: `"HVV_LISTED"`)





## Enum: CoordinateTypeEnum


* `4326` (value: `"EPSG_4326"`)

* `31466` (value: `"EPSG_31466"`)

* `31467` (value: `"EPSG_31467"`)

* `31468` (value: `"EPSG_31468"`)

* `31469` (value: `"EPSG_31469"`)





## Enum: RealtimeEnum


* `PLANDATA` (value: `"PLANDATA"`)

* `REALTIME` (value: `"REALTIME"`)

* `AUTO` (value: `"AUTO"`)





## Enum: ToStartByEnum


* `BUS` (value: `"BUS"`)

* `TRAIN` (value: `"TRAIN"`)

* `SHIP` (value: `"SHIP"`)

* `FOOTPATH` (value: `"FOOTPATH"`)

* `BICYCLE` (value: `"BICYCLE"`)

* `AIRPLANE` (value: `"AIRPLANE"`)

* `CHANGE` (value: `"CHANGE"`)

* `CHANGE_SAME_PLATFORM` (value: `"CHANGE_SAME_PLATFORM"`)





## Enum: ToDestByEnum


* `BUS` (value: `"BUS"`)

* `TRAIN` (value: `"TRAIN"`)

* `SHIP` (value: `"SHIP"`)

* `FOOTPATH` (value: `"FOOTPATH"`)

* `BICYCLE` (value: `"BICYCLE"`)

* `AIRPLANE` (value: `"AIRPLANE"`)

* `CHANGE` (value: `"CHANGE"`)

* `CHANGE_SAME_PLATFORM` (value: `"CHANGE_SAME_PLATFORM"`)




