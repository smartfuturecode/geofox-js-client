# GeofoxApi.DLResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**returnCode** | **String** |  | 
**errorText** | **String** |  | [optional] 
**errorDevInfo** | **String** |  | [optional] 
**time** | [**GTITime**](GTITime.md) |  | 
**departures** | [**[Departure]**](Departure.md) |  | [optional] 
**filter** | [**[DLFilterEntry]**](DLFilterEntry.md) |  | [optional] 
**serviceTypes** | **[String]** |  | [optional] 



## Enum: ReturnCodeEnum


* `OK` (value: `"OK"`)

* `ERROR_ROUTE` (value: `"ERROR_ROUTE"`)

* `ERROR_COMM` (value: `"ERROR_COMM"`)

* `ERROR_CN_TOO_MANY` (value: `"ERROR_CN_TOO_MANY"`)

* `ERROR_TEXT` (value: `"ERROR_TEXT"`)

* `START_DEST_TOO_CLOSE` (value: `"START_DEST_TOO_CLOSE"`)





## Enum: [ServiceTypesEnum]


* `ZUG` (value: `"ZUG"`)

* `UBAHN` (value: `"UBAHN"`)

* `SBAHN` (value: `"SBAHN"`)

* `AKN` (value: `"AKN"`)

* `RBAHN` (value: `"RBAHN"`)

* `FERNBAHN` (value: `"FERNBAHN"`)

* `BUS` (value: `"BUS"`)

* `STADTBUS` (value: `"STADTBUS"`)

* `METROBUS` (value: `"METROBUS"`)

* `SCHNELLBUS` (value: `"SCHNELLBUS"`)

* `NACHTBUS` (value: `"NACHTBUS"`)

* `XPRESSBUS` (value: `"XPRESSBUS"`)

* `EILBUS` (value: `"EILBUS"`)

* `AST` (value: `"AST"`)

* `FAEHRE` (value: `"FAEHRE"`)




