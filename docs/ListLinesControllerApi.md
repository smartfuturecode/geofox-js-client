# GeofoxApi.ListLinesControllerApi

All URIs are relative to *https://gti.geofox.de*

Method | HTTP request | Description
------------- | ------------- | -------------
[**listLines**](ListLinesControllerApi.md#listLines) | **POST** /gti/public/listLines | 



## listLines

> LLResponse listLines(lLRequest)



### Example

```javascript
import GeofoxApi from 'geofox_api';
let defaultClient = GeofoxApi.ApiClient.instance;
// Configure HTTP basic authorization: APIKey1
let APIKey1 = defaultClient.authentications['APIKey1'];
APIKey1.username = 'YOUR USERNAME';
APIKey1.password = 'YOUR PASSWORD';

let apiInstance = new GeofoxApi.ListLinesControllerApi();
let lLRequest = new GeofoxApi.LLRequest(); // LLRequest | 
apiInstance.listLines(lLRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lLRequest** | [**LLRequest**](LLRequest.md)|  | 

### Return type

[**LLResponse**](LLResponse.md)

### Authorization

[APIKey1](../README.md#APIKey1)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: */*

