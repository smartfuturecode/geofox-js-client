# GeofoxApi.StationInformationResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**returnCode** | **String** |  | 
**errorText** | **String** |  | [optional] 
**errorDevInfo** | **String** |  | [optional] 
**partialStations** | [**[PartialStation]**](PartialStation.md) |  | [optional] 
**lastUpdate** | [**GTITime**](GTITime.md) |  | [optional] 



## Enum: ReturnCodeEnum


* `OK` (value: `"OK"`)

* `ERROR_ROUTE` (value: `"ERROR_ROUTE"`)

* `ERROR_COMM` (value: `"ERROR_COMM"`)

* `ERROR_CN_TOO_MANY` (value: `"ERROR_CN_TOO_MANY"`)

* `ERROR_TEXT` (value: `"ERROR_TEXT"`)

* `START_DEST_TOO_CLOSE` (value: `"START_DEST_TOO_CLOSE"`)




