# GeofoxApi.TariffOptimizerTicket

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tariffKindId** | **Number** |  | [optional] 
**tariffKindLabel** | **String** |  | [optional] 
**tariffLevelId** | **Number** |  | [optional] 
**tariffLevelLabel** | **String** |  | [optional] 
**tariffRegions** | **[String]** |  | 
**regionType** | **String** |  | 
**count** | **Number** |  | [optional] 
**extraFare** | **Boolean** |  | [optional] 
**personType** | **String** |  | 
**centPrice** | **Number** |  | [optional] 



## Enum: RegionTypeEnum


* `RING` (value: `"RING"`)

* `ZONE` (value: `"ZONE"`)

* `COUNTY` (value: `"COUNTY"`)

* `GH_ZONE` (value: `"GH_ZONE"`)





## Enum: PersonTypeEnum


* `ALL` (value: `"ALL"`)

* `ADULT` (value: `"ADULT"`)

* `ELDERLY` (value: `"ELDERLY"`)

* `APPRENTICE` (value: `"APPRENTICE"`)

* `PUPIL` (value: `"PUPIL"`)

* `STUDENT` (value: `"STUDENT"`)

* `CHILD` (value: `"CHILD"`)




