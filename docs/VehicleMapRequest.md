# GeofoxApi.VehicleMapRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**language** | **String** |  | [optional] [default to &#39;de&#39;]
**version** | **Number** |  | [optional] [default to 38]
**filterType** | **String** |  | [default to &#39;NO_FILTER&#39;]
**boundingBox** | [**BoundingBox**](BoundingBox.md) |  | 
**periodBegin** | **Number** |  | [optional] 
**periodEnd** | **Number** |  | [optional] 
**withoutCoords** | **Boolean** |  | [optional] 
**coordinateType** | **String** |  | [optional] [default to &#39;EPSG_4326&#39;]
**vehicleTypes** | **[String]** |  | [optional] 
**realtime** | **Boolean** |  | [optional] 



## Enum: FilterTypeEnum


* `NO_FILTER` (value: `"NO_FILTER"`)

* `HVV_LISTED` (value: `"HVV_LISTED"`)





## Enum: CoordinateTypeEnum


* `4326` (value: `"EPSG_4326"`)

* `31466` (value: `"EPSG_31466"`)

* `31467` (value: `"EPSG_31467"`)

* `31468` (value: `"EPSG_31468"`)

* `31469` (value: `"EPSG_31469"`)





## Enum: [VehicleTypesEnum]


* `REGIONALBUS` (value: `"REGIONALBUS"`)

* `METROBUS` (value: `"METROBUS"`)

* `NACHTBUS` (value: `"NACHTBUS"`)

* `SCHNELLBUS` (value: `"SCHNELLBUS"`)

* `XPRESSBUS` (value: `"XPRESSBUS"`)

* `AST` (value: `"AST"`)

* `SCHIFF` (value: `"SCHIFF"`)

* `U_BAHN` (value: `"U_BAHN"`)

* `S_BAHN` (value: `"S_BAHN"`)

* `A_BAHN` (value: `"A_BAHN"`)

* `R_BAHN` (value: `"R_BAHN"`)

* `F_BAHN` (value: `"F_BAHN"`)

* `EILBUS` (value: `"EILBUS"`)




