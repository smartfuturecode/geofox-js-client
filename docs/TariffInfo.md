# GeofoxApi.TariffInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tariffName** | **String** |  | 
**tariffRegions** | [**[TariffRegionInfo]**](TariffRegionInfo.md) |  | [optional] 
**extraFareType** | **String** |  | [optional] [default to &#39;NO&#39;]
**ticketInfos** | [**[TicketInfo]**](TicketInfo.md) |  | [optional] 
**ticketRemarks** | **String** |  | [optional] 



## Enum: ExtraFareTypeEnum


* `NO` (value: `"NO"`)

* `POSSIBLE` (value: `"POSSIBLE"`)

* `REQUIRED` (value: `"REQUIRED"`)




