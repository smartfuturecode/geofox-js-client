# GeofoxApi.TariffZoneNeighboursControllerApi

All URIs are relative to *https://gti.geofox.de*

Method | HTTP request | Description
------------- | ------------- | -------------
[**tariffZoneNeighbours**](TariffZoneNeighboursControllerApi.md#tariffZoneNeighbours) | **POST** /gti/public/tariffZoneNeighbours | 



## tariffZoneNeighbours

> TariffZoneNeighboursResponse tariffZoneNeighbours()



### Example

```javascript
import GeofoxApi from 'geofox_api';
let defaultClient = GeofoxApi.ApiClient.instance;
// Configure HTTP basic authorization: APIKey1
let APIKey1 = defaultClient.authentications['APIKey1'];
APIKey1.username = 'YOUR USERNAME';
APIKey1.password = 'YOUR PASSWORD';

let apiInstance = new GeofoxApi.TariffZoneNeighboursControllerApi();
apiInstance.tariffZoneNeighbours((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**TariffZoneNeighboursResponse**](TariffZoneNeighboursResponse.md)

### Authorization

[APIKey1](../README.md#APIKey1)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

