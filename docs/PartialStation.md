# GeofoxApi.PartialStation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lines** | **[String]** |  | [optional] 
**stationOutline** | **String** |  | [optional] 
**elevators** | [**[Elevator]**](Elevator.md) |  | [optional] 


