# GeofoxApi.TariffRegionInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**regionType** | **String** |  | 
**alternatives** | [**[TariffRegionList]**](TariffRegionList.md) |  | [optional] 



## Enum: RegionTypeEnum


* `ZONE` (value: `"ZONE"`)

* `GH_ZONE` (value: `"GH_ZONE"`)

* `RING` (value: `"RING"`)

* `COUNTY` (value: `"COUNTY"`)

* `GH` (value: `"GH"`)

* `NET` (value: `"NET"`)

* `ZG` (value: `"ZG"`)

* `STADTVERKEHR` (value: `"STADTVERKEHR"`)




