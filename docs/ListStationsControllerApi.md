# GeofoxApi.ListStationsControllerApi

All URIs are relative to *https://gti.geofox.de*

Method | HTTP request | Description
------------- | ------------- | -------------
[**listStations**](ListStationsControllerApi.md#listStations) | **POST** /gti/public/listStations | 



## listStations

> LSResponse listStations(lSRequest)



### Example

```javascript
import GeofoxApi from 'geofox_api';
let defaultClient = GeofoxApi.ApiClient.instance;
// Configure HTTP basic authorization: APIKey1
let APIKey1 = defaultClient.authentications['APIKey1'];
APIKey1.username = 'YOUR USERNAME';
APIKey1.password = 'YOUR PASSWORD';

let apiInstance = new GeofoxApi.ListStationsControllerApi();
let lSRequest = new GeofoxApi.LSRequest(); // LSRequest | 
apiInstance.listStations(lSRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lSRequest** | [**LSRequest**](LSRequest.md)|  | 

### Return type

[**LSResponse**](LSResponse.md)

### Authorization

[APIKey1](../README.md#APIKey1)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: */*

