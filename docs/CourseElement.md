# GeofoxApi.CourseElement

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fromStation** | [**SDName**](SDName.md) |  | 
**fromPlatform** | **String** |  | [optional] 
**fromRealtimePlatform** | **String** |  | [optional] 
**toStation** | [**SDName**](SDName.md) |  | 
**toPlatform** | **String** |  | [optional] 
**toRealtimePlatform** | **String** |  | [optional] 
**model** | **String** |  | [optional] 
**depTime** | **Date** |  | 
**arrTime** | **Date** |  | 
**depDelay** | **Number** |  | [optional] 
**arrDelay** | **Number** |  | [optional] 
**fromExtra** | **Boolean** |  | [optional] [default to false]
**fromCancelled** | **Boolean** |  | [optional] [default to false]
**toExtra** | **Boolean** |  | [optional] [default to false]
**toCancelled** | **Boolean** |  | [optional] [default to false]
**attributes** | [**[Attribute]**](Attribute.md) |  | [optional] 
**path** | [**Path**](Path.md) |  | [optional] 


