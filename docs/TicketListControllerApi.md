# GeofoxApi.TicketListControllerApi

All URIs are relative to *https://gti.geofox.de*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ticketList**](TicketListControllerApi.md#ticketList) | **POST** /gti/public/ticketList | 



## ticketList

> TicketListResponse ticketList(ticketListRequest)



### Example

```javascript
import GeofoxApi from 'geofox_api';
let defaultClient = GeofoxApi.ApiClient.instance;
// Configure HTTP basic authorization: APIKey1
let APIKey1 = defaultClient.authentications['APIKey1'];
APIKey1.username = 'YOUR USERNAME';
APIKey1.password = 'YOUR PASSWORD';

let apiInstance = new GeofoxApi.TicketListControllerApi();
let ticketListRequest = new GeofoxApi.TicketListRequest(); // TicketListRequest | 
apiInstance.ticketList(ticketListRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticketListRequest** | [**TicketListRequest**](TicketListRequest.md)|  | 

### Return type

[**TicketListResponse**](TicketListResponse.md)

### Authorization

[APIKey1](../README.md#APIKey1)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: */*

