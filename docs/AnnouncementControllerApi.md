# GeofoxApi.AnnouncementControllerApi

All URIs are relative to *https://gti.geofox.de*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAnnouncements**](AnnouncementControllerApi.md#getAnnouncements) | **POST** /gti/public/getAnnouncements | 



## getAnnouncements

> AnnouncementResponse getAnnouncements(announcementRequest)



### Example

```javascript
import GeofoxApi from 'geofox_api';
let defaultClient = GeofoxApi.ApiClient.instance;
// Configure HTTP basic authorization: APIKey1
let APIKey1 = defaultClient.authentications['APIKey1'];
APIKey1.username = 'YOUR USERNAME';
APIKey1.password = 'YOUR PASSWORD';

let apiInstance = new GeofoxApi.AnnouncementControllerApi();
let announcementRequest = new GeofoxApi.AnnouncementRequest(); // AnnouncementRequest | 
apiInstance.getAnnouncements(announcementRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **announcementRequest** | [**AnnouncementRequest**](AnnouncementRequest.md)|  | 

### Return type

[**AnnouncementResponse**](AnnouncementResponse.md)

### Authorization

[APIKey1](../README.md#APIKey1)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: */*

