# GeofoxApi.DCRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**language** | **String** |  | [optional] [default to &#39;de&#39;]
**version** | **Number** |  | [optional] [default to 38]
**filterType** | **String** |  | [default to &#39;NO_FILTER&#39;]
**lineKey** | **String** |  |
**station** | [**SDName**](SDName.md) |  |
**time** | **Date** |  | [optional] 
**direction** | **String** |  | [optional]
**origin** | **String** |  | [optional]
**serviceId** | **Number** |  | [optional] [default to -1]
**segments** | **String** |  | [optional] [default to &#39;ALL&#39;]
**showPath** | **Boolean** |  | [optional] [default to false]
**coordinateType** | **String** |  | [optional] [default to &#39;EPSG_4326&#39;]



## Enum: FilterTypeEnum


* `NO_FILTER` (value: `"NO_FILTER"`)

* `HVV_LISTED` (value: `"HVV_LISTED"`)





## Enum: SegmentsEnum


* `BEFORE` (value: `"BEFORE"`)

* `AFTER` (value: `"AFTER"`)

* `ALL` (value: `"ALL"`)





## Enum: CoordinateTypeEnum


* `4326` (value: `"EPSG_4326"`)

* `31466` (value: `"EPSG_31466"`)

* `31467` (value: `"EPSG_31467"`)

* `31468` (value: `"EPSG_31468"`)

* `31469` (value: `"EPSG_31469"`)
