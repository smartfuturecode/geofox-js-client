# GeofoxApi.TariffKind

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**label** | **String** |  | 
**requiresPersonType** | **Boolean** |  | [optional] [default to false]
**ticketType** | **String** |  | [optional] 
**levelCombinations** | **[Number]** |  | [optional] 



## Enum: TicketTypeEnum


* `OCCASIONAL_TICKET` (value: `"OCCASIONAL_TICKET"`)

* `SEASON_TICKET` (value: `"SEASON_TICKET"`)




