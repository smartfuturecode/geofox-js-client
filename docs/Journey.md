# GeofoxApi.Journey

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**journeyID** | **String** |  | 
**line** | [**Service**](Service.md) |  | 
**vehicleType** | **String** |  | 
**realtime** | **Boolean** |  | [optional] 
**segments** | [**[PathSegment]**](PathSegment.md) |  | [optional] 



## Enum: VehicleTypeEnum


* `REGIONALBUS` (value: `"REGIONALBUS"`)

* `METROBUS` (value: `"METROBUS"`)

* `NACHTBUS` (value: `"NACHTBUS"`)

* `SCHNELLBUS` (value: `"SCHNELLBUS"`)

* `XPRESSBUS` (value: `"XPRESSBUS"`)

* `AST` (value: `"AST"`)

* `SCHIFF` (value: `"SCHIFF"`)

* `U_BAHN` (value: `"U_BAHN"`)

* `S_BAHN` (value: `"S_BAHN"`)

* `A_BAHN` (value: `"A_BAHN"`)

* `R_BAHN` (value: `"R_BAHN"`)

* `F_BAHN` (value: `"F_BAHN"`)

* `EILBUS` (value: `"EILBUS"`)




