# GeofoxApi.TariffRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**language** | **String** |  | [optional] [default to &#39;de&#39;]
**version** | **Number** |  | [optional] [default to 38]
**filterType** | **String** |  | [default to &#39;NO_FILTER&#39;]
**scheduleElements** | [**[ScheduleElementLight]**](ScheduleElementLight.md) |  | 
**departure** | [**GTITime**](GTITime.md) |  | 
**arrival** | [**GTITime**](GTITime.md) |  | 
**returnReduced** | **Boolean** |  | [optional] [default to false]
**returnPartialTickets** | **Boolean** |  | [optional] [default to true]



## Enum: FilterTypeEnum


* `NO_FILTER` (value: `"NO_FILTER"`)

* `HVV_LISTED` (value: `"HVV_LISTED"`)




