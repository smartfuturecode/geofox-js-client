# GeofoxApi.Service

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**direction** | **String** |  | [optional] 
**directionId** | **Number** |  | [optional] 
**origin** | **String** |  | [optional] 
**type** | [**ServiceType**](ServiceType.md) |  | 
**id** | **String** |  | [optional] 


