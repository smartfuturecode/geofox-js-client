# GeofoxApi.Attribute

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **String** |  | 
**isPlanned** | **Boolean** |  | [optional] 
**value** | **String** |  | 
**types** | **[String]** |  | [optional] 


