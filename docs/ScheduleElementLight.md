# GeofoxApi.ScheduleElementLight

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**departureStationId** | **String** |  | 
**arrivalStationId** | **String** |  | 
**lineId** | **String** |  | 


