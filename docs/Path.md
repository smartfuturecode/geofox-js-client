# GeofoxApi.Path

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**track** | [**[Coordinate]**](Coordinate.md) |  | 
**attributes** | **[String]** |  | [optional] 


