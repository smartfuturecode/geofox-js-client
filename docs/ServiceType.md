# GeofoxApi.ServiceType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**simpleType** | **String** |  | 
**shortInfo** | **String** |  | [optional] 
**longInfo** | **String** |  | [optional] 
**model** | **String** |  | [optional] 



## Enum: SimpleTypeEnum


* `BUS` (value: `"BUS"`)

* `TRAIN` (value: `"TRAIN"`)

* `SHIP` (value: `"SHIP"`)

* `FOOTPATH` (value: `"FOOTPATH"`)

* `BICYCLE` (value: `"BICYCLE"`)

* `AIRPLANE` (value: `"AIRPLANE"`)

* `CHANGE` (value: `"CHANGE"`)

* `CHANGE_SAME_PLATFORM` (value: `"CHANGE_SAME_PLATFORM"`)




