# GeofoxApi.IndividualRouteControllerApi

All URIs are relative to *https://gti.geofox.de*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getIndividualRoute**](IndividualRouteControllerApi.md#getIndividualRoute) | **POST** /gti/public/getIndividualRoute | 



## getIndividualRoute

> IndividualRouteResponse getIndividualRoute(individualRouteRequest)



### Example

```javascript
import GeofoxApi from 'geofox_api';
let defaultClient = GeofoxApi.ApiClient.instance;
// Configure HTTP basic authorization: APIKey1
let APIKey1 = defaultClient.authentications['APIKey1'];
APIKey1.username = 'YOUR USERNAME';
APIKey1.password = 'YOUR PASSWORD';

let apiInstance = new GeofoxApi.IndividualRouteControllerApi();
let individualRouteRequest = new GeofoxApi.IndividualRouteRequest(); // IndividualRouteRequest | 
apiInstance.getIndividualRoute(individualRouteRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **individualRouteRequest** | [**IndividualRouteRequest**](IndividualRouteRequest.md)|  | 

### Return type

[**IndividualRouteResponse**](IndividualRouteResponse.md)

### Authorization

[APIKey1](../README.md#APIKey1)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: */*

