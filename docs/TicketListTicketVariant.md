# GeofoxApi.TicketListTicketVariant

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ticketId** | **Number** |  | [optional] 
**kaNummer** | **Number** |  | [optional] 
**price** | **Number** |  | [optional] 
**currency** | **String** |  | [optional] [default to &#39;EUR&#39;]
**ticketClass** | **String** |  | 
**discount** | **String** |  | 
**validityBegin** | **Date** |  | 
**validityEnd** | **Date** |  | 



## Enum: TicketClassEnum


* `NONE` (value: `"NONE"`)

* `SECOND` (value: `"SECOND"`)

* `FIRST` (value: `"FIRST"`)

* `SCHNELL` (value: `"SCHNELL"`)





## Enum: DiscountEnum


* `NONE` (value: `"NONE"`)

* `ONLINE` (value: `"ONLINE"`)

* `SOCIAL` (value: `"SOCIAL"`)




