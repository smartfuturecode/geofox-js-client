# GeofoxApi.TariffInfoSelector

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tariff** | **String** |  | [optional] [default to &#39;HVV&#39;]
**tariffRegions** | **Boolean** |  | [optional] [default to true]
**kinds** | **[Number]** |  | [optional] 


