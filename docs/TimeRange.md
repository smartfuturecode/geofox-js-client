# GeofoxApi.TimeRange

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**begin** | **Date** |  | [optional] 
**end** | **Date** |  | [optional] 


