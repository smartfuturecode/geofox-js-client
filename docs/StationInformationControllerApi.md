# GeofoxApi.StationInformationControllerApi

All URIs are relative to *https://gti.geofox.de*

Method | HTTP request | Description
------------- | ------------- | -------------
[**tariffMetaData**](StationInformationControllerApi.md#tariffMetaData) | **POST** /gti/public/getStationInformation | 



## tariffMetaData

> StationInformationResponse tariffMetaData(stationInformationRequest)



### Example

```javascript
import GeofoxApi from 'geofox_api';
let defaultClient = GeofoxApi.ApiClient.instance;
// Configure HTTP basic authorization: APIKey1
let APIKey1 = defaultClient.authentications['APIKey1'];
APIKey1.username = 'YOUR USERNAME';
APIKey1.password = 'YOUR PASSWORD';

let apiInstance = new GeofoxApi.StationInformationControllerApi();
let stationInformationRequest = new GeofoxApi.StationInformationRequest(); // StationInformationRequest | 
apiInstance.tariffMetaData(stationInformationRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **stationInformationRequest** | [**StationInformationRequest**](StationInformationRequest.md)|  | 

### Return type

[**StationInformationResponse**](StationInformationResponse.md)

### Authorization

[APIKey1](../README.md#APIKey1)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: */*

