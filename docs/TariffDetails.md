# GeofoxApi.TariffDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**innerCity** | **Boolean** |  | [optional] 
**city** | **Boolean** |  | [optional] 
**cityTraffic** | **Boolean** |  | [optional] 
**gratis** | **Boolean** |  | [optional] 
**greaterArea** | **Boolean** |  | [optional] 
**tariffZones** | **[Number]** |  | [optional] 
**regions** | **[Number]** |  | [optional] 
**counties** | **[String]** |  | [optional] 
**rings** | **[String]** |  | [optional] 
**fareStage** | **Boolean** |  | [optional] 
**fareStageNumber** | **Number** |  | [optional] 
**tariffNames** | **[String]** |  | [optional] 
**uniqueValues** | **Boolean** |  | [optional] 


