# GeofoxApi.TimePeriod

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**begin** | **String** |  | 
**end** | **String** |  | 


