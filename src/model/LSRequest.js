/**
 * Geofox Api
 * Das GEOFOX Thin Interface, im folgenden GTI genannt, ist eine REST-ähnliche Web-Service-Schnittstelle für GEOFOX.
 *
 * The version of the OpenAPI document: 38.1.0
 * Contact: api@geofox.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The LSRequest model module.
 * @module model/LSRequest
 * @version 38.1.0
 */
class LSRequest {
    /**
     * Constructs a new <code>LSRequest</code>.
     * @alias module:model/LSRequest
     * @param filterType {module:model/LSRequest.FilterTypeEnum} 
     */
    constructor(filterType) { 
        
        LSRequest.initialize(this, filterType);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, filterType) { 
        obj['filterType'] = filterType;
    }

    /**
     * Constructs a <code>LSRequest</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/LSRequest} obj Optional instance to populate.
     * @return {module:model/LSRequest} The populated <code>LSRequest</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new LSRequest();

            if (data.hasOwnProperty('language')) {
                obj['language'] = ApiClient.convertToType(data['language'], 'String');
            }
            if (data.hasOwnProperty('version')) {
                obj['version'] = ApiClient.convertToType(data['version'], 'Number');
            }
            if (data.hasOwnProperty('filterType')) {
                obj['filterType'] = ApiClient.convertToType(data['filterType'], 'String');
            }
            if (data.hasOwnProperty('dataReleaseID')) {
                obj['dataReleaseID'] = ApiClient.convertToType(data['dataReleaseID'], 'String');
            }
            if (data.hasOwnProperty('modificationTypes')) {
                obj['modificationTypes'] = ApiClient.convertToType(data['modificationTypes'], ['String']);
            }
            if (data.hasOwnProperty('coordinateType')) {
                obj['coordinateType'] = ApiClient.convertToType(data['coordinateType'], 'String');
            }
            if (data.hasOwnProperty('filterEquivalent')) {
                obj['filterEquivalent'] = ApiClient.convertToType(data['filterEquivalent'], 'Boolean');
            }
        }
        return obj;
    }


}

/**
 * @member {String} language
 * @default 'de'
 */
LSRequest.prototype['language'] = 'de';

/**
 * @member {Number} version
 * @default 38
 */
LSRequest.prototype['version'] = 38;

/**
 * @member {module:model/LSRequest.FilterTypeEnum} filterType
 * @default 'NO_FILTER'
 */
LSRequest.prototype['filterType'] = 'NO_FILTER';

/**
 * @member {String} dataReleaseID
 */
LSRequest.prototype['dataReleaseID'] = undefined;

/**
 * @member {Array.<module:model/LSRequest.ModificationTypesEnum>} modificationTypes
 */
LSRequest.prototype['modificationTypes'] = undefined;

/**
 * @member {module:model/LSRequest.CoordinateTypeEnum} coordinateType
 * @default 'EPSG_4326'
 */
LSRequest.prototype['coordinateType'] = 'EPSG_4326';

/**
 * @member {Boolean} filterEquivalent
 * @default false
 */
LSRequest.prototype['filterEquivalent'] = false;





/**
 * Allowed values for the <code>filterType</code> property.
 * @enum {String}
 * @readonly
 */
LSRequest['FilterTypeEnum'] = {

    /**
     * value: "NO_FILTER"
     * @const
     */
    "NO_FILTER": "NO_FILTER",

    /**
     * value: "HVV_LISTED"
     * @const
     */
    "HVV_LISTED": "HVV_LISTED"
};


/**
 * Allowed values for the <code>modificationTypes</code> property.
 * @enum {String}
 * @readonly
 */
LSRequest['ModificationTypesEnum'] = {

    /**
     * value: "MAIN"
     * @const
     */
    "MAIN": "MAIN",

    /**
     * value: "POSITION"
     * @const
     */
    "POSITION": "POSITION"
};


/**
 * Allowed values for the <code>coordinateType</code> property.
 * @enum {String}
 * @readonly
 */
LSRequest['CoordinateTypeEnum'] = {

    /**
     * value: "EPSG_4326"
     * @const
     */
    "4326": "EPSG_4326",

    /**
     * value: "EPSG_31466"
     * @const
     */
    "31466": "EPSG_31466",

    /**
     * value: "EPSG_31467"
     * @const
     */
    "31467": "EPSG_31467",

    /**
     * value: "EPSG_31468"
     * @const
     */
    "31468": "EPSG_31468",

    /**
     * value: "EPSG_31469"
     * @const
     */
    "31469": "EPSG_31469"
};



export default LSRequest;

