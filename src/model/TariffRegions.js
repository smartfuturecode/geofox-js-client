/**
 * Geofox Api
 * Das GEOFOX Thin Interface, im folgenden GTI genannt, ist eine REST-ähnliche Web-Service-Schnittstelle für GEOFOX.
 *
 * The version of the OpenAPI document: 38.1.0
 * Contact: api@geofox.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The TariffRegions model module.
 * @module model/TariffRegions
 * @version 38.1.0
 */
class TariffRegions {
    /**
     * Constructs a new <code>TariffRegions</code>.
     * @alias module:model/TariffRegions
     * @param regions {Array.<String>} 
     */
    constructor(regions) { 
        
        TariffRegions.initialize(this, regions);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, regions) { 
        obj['regions'] = regions;
    }

    /**
     * Constructs a <code>TariffRegions</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/TariffRegions} obj Optional instance to populate.
     * @return {module:model/TariffRegions} The populated <code>TariffRegions</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new TariffRegions();

            if (data.hasOwnProperty('regions')) {
                obj['regions'] = ApiClient.convertToType(data['regions'], ['String']);
            }
        }
        return obj;
    }


}

/**
 * @member {Array.<String>} regions
 */
TariffRegions.prototype['regions'] = undefined;






export default TariffRegions;

