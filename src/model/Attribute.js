/**
 * Geofox Api
 * Das GEOFOX Thin Interface, im folgenden GTI genannt, ist eine REST-ähnliche Web-Service-Schnittstelle für GEOFOX.
 *
 * The version of the OpenAPI document: 38.1.0
 * Contact: api@geofox.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The Attribute model module.
 * @module model/Attribute
 * @version 38.1.0
 */
class Attribute {
    /**
     * Constructs a new <code>Attribute</code>.
     * @alias module:model/Attribute
     * @param title {String} 
     * @param value {String} 
     */
    constructor(title, value) { 
        
        Attribute.initialize(this, title, value);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, title, value) { 
        obj['title'] = title;
        obj['value'] = value;
    }

    /**
     * Constructs a <code>Attribute</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/Attribute} obj Optional instance to populate.
     * @return {module:model/Attribute} The populated <code>Attribute</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new Attribute();

            if (data.hasOwnProperty('title')) {
                obj['title'] = ApiClient.convertToType(data['title'], 'String');
            }
            if (data.hasOwnProperty('isPlanned')) {
                obj['isPlanned'] = ApiClient.convertToType(data['isPlanned'], 'Boolean');
            }
            if (data.hasOwnProperty('value')) {
                obj['value'] = ApiClient.convertToType(data['value'], 'String');
            }
            if (data.hasOwnProperty('types')) {
                obj['types'] = ApiClient.convertToType(data['types'], ['String']);
            }
        }
        return obj;
    }


}

/**
 * @member {String} title
 */
Attribute.prototype['title'] = undefined;

/**
 * @member {Boolean} isPlanned
 */
Attribute.prototype['isPlanned'] = undefined;

/**
 * @member {String} value
 */
Attribute.prototype['value'] = undefined;

/**
 * @member {Array.<String>} types
 */
Attribute.prototype['types'] = undefined;






export default Attribute;

