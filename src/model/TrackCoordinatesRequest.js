/**
 * Geofox Api
 * Das GEOFOX Thin Interface, im folgenden GTI genannt, ist eine REST-ähnliche Web-Service-Schnittstelle für GEOFOX.
 *
 * The version of the OpenAPI document: 38.1.0
 * Contact: api@geofox.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The TrackCoordinatesRequest model module.
 * @module model/TrackCoordinatesRequest
 * @version 38.1.0
 */
class TrackCoordinatesRequest {
    /**
     * Constructs a new <code>TrackCoordinatesRequest</code>.
     * @alias module:model/TrackCoordinatesRequest
     * @param filterType {module:model/TrackCoordinatesRequest.FilterTypeEnum} 
     * @param stopPointKeys {Array.<String>} 
     */
    constructor(filterType, stopPointKeys) { 
        
        TrackCoordinatesRequest.initialize(this, filterType, stopPointKeys);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, filterType, stopPointKeys) { 
        obj['filterType'] = filterType;
        obj['stopPointKeys'] = stopPointKeys;
    }

    /**
     * Constructs a <code>TrackCoordinatesRequest</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/TrackCoordinatesRequest} obj Optional instance to populate.
     * @return {module:model/TrackCoordinatesRequest} The populated <code>TrackCoordinatesRequest</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new TrackCoordinatesRequest();

            if (data.hasOwnProperty('language')) {
                obj['language'] = ApiClient.convertToType(data['language'], 'String');
            }
            if (data.hasOwnProperty('version')) {
                obj['version'] = ApiClient.convertToType(data['version'], 'Number');
            }
            if (data.hasOwnProperty('filterType')) {
                obj['filterType'] = ApiClient.convertToType(data['filterType'], 'String');
            }
            if (data.hasOwnProperty('coordinateType')) {
                obj['coordinateType'] = ApiClient.convertToType(data['coordinateType'], 'String');
            }
            if (data.hasOwnProperty('stopPointKeys')) {
                obj['stopPointKeys'] = ApiClient.convertToType(data['stopPointKeys'], ['String']);
            }
        }
        return obj;
    }


}

/**
 * @member {String} language
 * @default 'de'
 */
TrackCoordinatesRequest.prototype['language'] = 'de';

/**
 * @member {Number} version
 * @default 38
 */
TrackCoordinatesRequest.prototype['version'] = 38;

/**
 * @member {module:model/TrackCoordinatesRequest.FilterTypeEnum} filterType
 * @default 'NO_FILTER'
 */
TrackCoordinatesRequest.prototype['filterType'] = 'NO_FILTER';

/**
 * @member {module:model/TrackCoordinatesRequest.CoordinateTypeEnum} coordinateType
 * @default 'EPSG_4326'
 */
TrackCoordinatesRequest.prototype['coordinateType'] = 'EPSG_4326';

/**
 * @member {Array.<String>} stopPointKeys
 */
TrackCoordinatesRequest.prototype['stopPointKeys'] = undefined;





/**
 * Allowed values for the <code>filterType</code> property.
 * @enum {String}
 * @readonly
 */
TrackCoordinatesRequest['FilterTypeEnum'] = {

    /**
     * value: "NO_FILTER"
     * @const
     */
    "NO_FILTER": "NO_FILTER",

    /**
     * value: "HVV_LISTED"
     * @const
     */
    "HVV_LISTED": "HVV_LISTED"
};


/**
 * Allowed values for the <code>coordinateType</code> property.
 * @enum {String}
 * @readonly
 */
TrackCoordinatesRequest['CoordinateTypeEnum'] = {

    /**
     * value: "EPSG_4326"
     * @const
     */
    "4326": "EPSG_4326",

    /**
     * value: "EPSG_31466"
     * @const
     */
    "31466": "EPSG_31466",

    /**
     * value: "EPSG_31467"
     * @const
     */
    "31467": "EPSG_31467",

    /**
     * value: "EPSG_31468"
     * @const
     */
    "31468": "EPSG_31468",

    /**
     * value: "EPSG_31469"
     * @const
     */
    "31469": "EPSG_31469"
};



export default TrackCoordinatesRequest;

