'use strict';

function vehicleIconUrl (vehicles, size) {

  let output = [];

  vehicles.forEach((item, i) => {
    switch (item) {
    case 'REGIONALBUS' || 'METROBUS' || 'NACHTBUS' || 'XPRESSBUS' || 'EILBUS' || 'SCHNELLBUS':
      output.push('bus');
      break;
    case 'AST':
      output.push('ast');
      break;
    case 'SCHIFF':
      output.push('faehre');
      break;
    case 'U_BAHN':
      output.push('ubahn');
      break;
    case 'S_BAHN':
      output.push('sbahn');
      break;
    case 'A_BAHN':
      output.push('abahn');
      break;
    case 'R_BAHN':
      output.push('rbahn');
      break;
    case 'F_BAHN':
      output.push('fbahn');
      break;
    }
  });

  if (output.length > 4) {
    output = output.slice(0, 4);
  }
  return output.length === 0 ? '' : `https://cloud.geofox.de/icon/vehicle?types=${output.join(';')}&height=${size}&varSize=true"`;
}


function lineIconUrl (lineId, size) {
    return `https://cloud.geofox.de/icon/line?height=${size}&lineKey=${lineId}`

}
