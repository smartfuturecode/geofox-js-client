/**
 * Geofox Api
 * Das GEOFOX Thin Interface, im folgenden GTI genannt, ist eine REST-ähnliche Web-Service-Schnittstelle für GEOFOX.
 *
 * The version of the OpenAPI document: 38.1.0
 * Contact: api@geofox.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */


import ApiClient from "../ApiClient";
import TariffMetaDataRequest from '../model/TariffMetaDataRequest';
import TariffMetaDataResponse from '../model/TariffMetaDataResponse';

/**
* TariffMetaDataController service.
* @module api/TariffMetaDataControllerApi
* @version 38.1.0
*/
export default class TariffMetaDataControllerApi {

    /**
    * Constructs a new TariffMetaDataControllerApi. 
    * @alias module:api/TariffMetaDataControllerApi
    * @class
    * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
    * default to {@link module:ApiClient#instance} if unspecified.
    */
    constructor(apiClient) {
        this.apiClient = apiClient || ApiClient.instance;
    }


    /**
     * Callback function to receive the result of the tariffMetaData1 operation.
     * @callback module:api/TariffMetaDataControllerApi~tariffMetaData1Callback
     * @param {String} error Error message, if any.
     * @param {module:model/TariffMetaDataResponse} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * @param {module:model/TariffMetaDataRequest} tariffMetaDataRequest 
     * @param {module:api/TariffMetaDataControllerApi~tariffMetaData1Callback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/TariffMetaDataResponse}
     */
    tariffMetaData1(tariffMetaDataRequest, callback) {
      let postBody = tariffMetaDataRequest;
      // verify the required parameter 'tariffMetaDataRequest' is set
      if (tariffMetaDataRequest === undefined || tariffMetaDataRequest === null) {
        throw new Error("Missing the required parameter 'tariffMetaDataRequest' when calling tariffMetaData1");
      }

      let pathParams = {
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['APIKey1'];
      let contentTypes = ['application/json'];
      let accepts = ['*/*'];
      let returnType = TariffMetaDataResponse;
      return this.apiClient.callApi(
        '/gti/public/tariffMetaData', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }


}
